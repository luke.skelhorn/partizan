<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;



class ApiAuthController extends Controller
{
    public function signIn(Response $response, Request $request) {
        $credentials = $request->only('email','password');
    
        if(!Auth::attempt($credentials)){
            return response('Invalid',403);
        }
    
        $token = [
            'access_token' => Auth::user()->createToken('access_token')->accessToken
        ];
    
        return $token;
    }
}
